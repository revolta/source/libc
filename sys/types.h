#pragma once

#include <stddef.h>
#include <stdint.h>

typedef int pid_t;

/* Used for file sizes */
typedef uint32_t off_t;

/* File descriptor */
typedef int32_t fd_t;

/* Used for a count of bytes or and error indication */
typedef long ssize_t;