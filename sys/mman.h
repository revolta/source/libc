#pragma once

#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#endif

    /* Protection options */
	#define PROT_NONE			0x0		/* Page cannot be accessed */
	#define PROT_READ			0x1		/* Page can be read */
	#define PROT_WRITE			0x2		/* Page can be written */
	#define PROT_EXEC			0x4		/* Page can be executed */

	/* Flag options */
	#define MAP_SHARED			0x01	/* Share changes */
	#define MAP_PRIVATE			0x02	/* Changes are private */
	#define MAP_FIXED			0x10	/* Interpret 'addr' exactly */
	#define MAP_ANONYMOUS		0x20

    void * mmap( void *, size_t, int, int, int, off_t );

	int munmap( void *, size_t );

#ifdef __cplusplus
}
#endif