#pragma once

#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#endif

    int execv( const char *, char * const [] );

    int execve( const char *, char * const [], char * const [] );

    int execvp( const char *, char * const [] );

    pid_t fork( void );

    void * sbrk( intptr_t );

    ssize_t write( int, const void *, size_t );

    int close( int );

#ifdef __cplusplus
}
#endif