#pragma once

#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#endif

    int kill( pid_t pid, int signal );

#ifdef __cplusplus
}
#endif