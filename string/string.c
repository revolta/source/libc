#include <string.h>

int memcmp( const void * a, const void * b, size_t size ) {
    const unsigned char * first = (const unsigned char *) a;
    const unsigned char * second = (const unsigned char *) b;

    for( size_t i = 0; i < size; i++ ) {
        if( first[ i ] < second[ i ] ) {
            return -1;
        }
        else if( second[ i ] < first[ i ] ) {
            return 1;
        }
    }

    return 0;
}

void * memcpy( void * __restrict destination, const void * __restrict source, size_t size ) {
    unsigned char * dest = (unsigned char *) destination;
    const unsigned char * src = (const unsigned char *) source;

    for( size_t i = 0; i < size; i++ ) {
        dest[ i ] = src[ i ];
    }

    return destination;
}

void * memmove( void * destination, const void * source, size_t size ) {
    unsigned char * dest = (unsigned char *) destination;
    const unsigned char * src = (const unsigned char *) source;

    if( dest < src ) {
        for( size_t i = 0; i < size; i++ ) {
            dest[ i ] = src[ i ];
        }
    }
    else {
        for( size_t i = size; i != 0; i-- ) {
            dest[ (i - 1) ] = src[ (i-1) ];
        }
    }

    return destination;
}

void * memset( void * buffer, int value, size_t size ) {
    unsigned char * buff = (unsigned char *) buffer;

    for( size_t i = 0; i < size; i++ ) {
        buff[ i ] = (unsigned char) value;
    }

    return buffer;
}

char * strcpy( char * destination, const char * source ) {
	#ifndef __DISABLE_WARNINGS
	#warning "strcpy() not implemented yet."
	#endif
}

size_t strlen( const char * str ) {
    size_t length = 0;

    while( str[ length ] ) {
        length++;
    }

    return length;
}