#include <stdio.h>

/* stdio global constructor being called before application main() */
 __attribute__((constructor)) void _init_stdio( void ) {
	/* TODO: Initialize stdio - open stdout, stdin and stderr streams */
	#ifndef __DISABLE_WARNINGS
	#warning "_init_stdio() not implemented yet."
	#endif
}

 /* stdio global destructor being called after application main() */
 __attribute__((destructor)) void _fini_stdio( void ) {
	/* TODO: Initialize stdio - close stdout, stdin and stderr streams */
	#ifndef __DISABLE_WARNINGS
	#warning "_fini_stdio() not implemented yet."
	#endif
}

int fclose( FILE * stream ) {
	#ifndef __DISABLE_WARNINGS
	#warning "fclose() not implemented yet."
	#endif
}

int fflush( FILE * stream ) {
	#ifndef __DISABLE_WARNINGS
	#warning "fflush() not implemented yet."
	#endif
}

FILE * fopen( const char * filename, const char * mode ) {
	#ifndef __DISABLE_WARNINGS
	#warning "fopen() not implemented yet."
	#endif
}

int fprintf( FILE * stream, const char * format, ... ) {
	#ifndef __DISABLE_WARNINGS
	#warning "fprintf() not implemented yet."
	#endif
}

size_t fread( void * ptr, size_t size, size_t count, FILE * stream ) {
	#ifndef __DISABLE_WARNINGS
	#warning "fread() not implemented yet."
	#endif
}

int fseek( FILE * stream, long int offset, int origin ) {
	#ifndef __DISABLE_WARNINGS
	#warning "fseek() not implemented yet."
	#endif
}

long int ftell( FILE * stream ) {
	#ifndef __DISABLE_WARNINGS
	#warning "ftell() not implemented yet."
	#endif
}

size_t fwrite( const void * ptr, size_t size, size_t count, FILE * stream ) {
	#ifndef __DISABLE_WARNINGS
	#warning "fwrite() not implemented yet."
	#endif
}

void setbuf( FILE * stream, char * buffer ) {
	#ifndef __DISABLE_WARNINGS
		#warning "setbuf() not implemented yet."
	#endif
}

int vfprintf( FILE * stream, const char* format , va_list arg ) {
	#ifndef __DISABLE_WARNINGS
	#warning "vfprintf() not implemented yet."
	#endif
}