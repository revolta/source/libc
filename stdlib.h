#pragma once

#include <unistd.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

    void abort( void ) __attribute__((__noreturn__));

    void * malloc( size_t );

    void free( void * );

    int atexit( void (*)( void ) );

#ifdef __cplusplus
}
#endif