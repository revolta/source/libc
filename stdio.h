#pragma once

#include <sys/types.h>
#include <stdarg.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

    /* Seek constants for use with <fseek> (N1548-7.21.1-3) */
	#define SEEK_CUR	( 1 )
	#define SEEK_END	( 2 )
	#define SEEK_SET	( 3 )

	#define EOF 	 	( -1 )

	typedef struct {
		uint_least64_t offset; 	/* File position offset */
		int status; 			/* Multibyte parsing state (unused, reserved) */
	} fpos_t;

	typedef struct FILE_t FILE;

	struct FILE_t {
	    fd_t   fd;   					/* File descriptor */
	    int    flags;
	    char * buffer;					/* Pointer to buffer memory */
	    size_t buffer_size;				/* Size of buffer */
	    size_t buffer_current_index;	/* Index of current position in buffer */
	    size_t buffer_end;				/* Index of last pre-read character in buffer */
	    fpos_t fpos;					/* Offset and multibyte parsing state */
	    size_t ungetidx;				/* Number of ungetc()'ed characters */
	    //unsigned char ungetbuf[_PDCLIB_UNGETCBUFSIZE]; /* ungetc() buffer */
	    unsigned int status;			/* Status flags; see above */
	    /* multibyte parsing status to be added later */
	#ifndef __STDC_NO_THREADS__
	    //_PDCLIB_mtx_t mutex;      		/* Multithreading safety */
	#endif
	    char * filename; 				/* Name the current stream has been opened with */
	    FILE * next;					/* Pointer to next struct (internal) */
	};

	extern FILE * 	stderr;
	#define stderr 	stderr
	extern FILE * 	stdin;
	#define stdin 	stdin
	extern FILE * 	stdout;
	#define stdout 	stdout

    extern FILE * stderr;

    #define stderr stderr

    int fclose( FILE * );

    int fflush( FILE * );

    FILE * fopen( const char *, const char * );

    int fprintf( FILE *, const char *, ... );

    size_t fread( void *, size_t, size_t, FILE * );

    int fseek( FILE *, long, int );

    long ftell( FILE * );

    size_t fwrite( const void *, size_t, size_t, FILE * );

    void setbuf( FILE *, char * );

    int vfprintf( FILE *, const char *, va_list );

#ifdef __cplusplus
}
#endif