/*
 * main.c
 *
 *  Created on: 20. 2. 2020
 *      Author: martin
 */

#include <unistd.h>

#ifdef POSIX_MULTI_PROCESS
#include <stdlib.h>
#endif

/* Application entry point */
int main( int argc, char** argv );

/* SOURCE: https://stackoverflow.com/questions/15265295/understanding-the-libc-init-array */
/* Those symbols are defined in ldscripts provided by the toolchain */
extern void ( *__preinit_array_start[] )( void ) __attribute__((weak));
extern void ( *__preinit_array_end[] )( void ) __attribute__((weak));
extern void ( *__init_array_start[] )( void ) __attribute__((weak));
extern void ( *__init_array_end[] )( void ) __attribute__((weak));
extern void ( *__fini_array_start[] )( void ) __attribute__((weak));
extern void ( *__fini_array_end[] )( void ) __attribute__((weak));

/* _init() and _fini() are defined in crti.s and crtn.s ABI files and the content of those functions
 * is contained within crtbegin.o and crtend.o provided by the compiler. */
extern void _init( void ) __attribute__((weak));
extern void _fini( void ) __attribute__((weak));

void _libc_call_global_constructors( void ) {
	/* Call static initializers */
	if(__preinit_array_start && __preinit_array_end) {
		for(size_t i = 0; i < __preinit_array_end - __preinit_array_start; i++)	{
			(*__preinit_array_start[i])();
		}
		_init();
		for(size_t i = 0; i < __init_array_end - __init_array_start; i++) {
			(*__init_array_start[i])();
		}
	}
}

void _libc_call_global_destructors( void ) {
	if(__fini_array_start && __fini_array_end)	{
		for(size_t i = 0; i < __fini_array_end - __fini_array_start; i++) {
			(*__fini_array_start[i])();
		}
		_fini();
	}
}

/* Application entry called by crt0.s */
/* TODO: In SINGLE_PROCESS, shall there be a return value? On just 'void' */
int _entry( void ) {
	/* Call global constructors - init libc */
	_libc_call_global_constructors();

#ifdef POSIX_MULTI_PROCESS
	/* Return value */
	int retval = -1;
#endif

	/* Parse main arguments */
	int argc;
	char** args;

	/* TODO parse arguments from command line */

	/* Call application main() */
#ifdef POSIX_MULTI_PROCESS
	retval = main( argc, args );
#else
	main( argc, args );
#endif

	/* Call global destructors - fini libc */
	_libc_call_global_destructors();

	/* Exit syscall with application return value is applicable
	 * only for multi process environment */
#ifdef POSIX_MULTI_PROCESS
	/* Call exit syscall to exit the application */
	exit( retval );
#else
	/* TODO: How to end the single process? Just call wait()? */
#endif
}
