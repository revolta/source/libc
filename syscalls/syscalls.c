#include <sys/mman.h>
#include <unistd.h>

int kill( pid_t pid, int signal ) {
    #ifndef __DISABLE_WARNINGS
    #warning "kill() not implemented yet."
    #endif
	return 0;
}

void * mmap( void *, size_t, int, int, int, off_t ) {
    #ifndef __DISABLE_WARNINGS
    #warning "mmap() not implemented yet."
    #endif
}

int munmap( void *, size_t ) {
    #ifndef __DISABLE_WARNINGS
    #warning "munmap() not implemented yet."
    #endif
}

int open( const char * path, int flags, ... ) {
#if false
	using namespace ReVolta;

	/* TODO: Mode not used at all -> shall it be forwarded to the kernel within
	 * the data structure or not used at all and thus removed? */
#if false
	mode_t mode = 0;

	if( flags & O_CREAT ) {
		va_list list;
		va_start( list, flags );
		mode = va_arg( list, mode_t );
		va_end( list );
	}
#endif
	/* Make up the data exchange data structure */
	Kernel::SyscallData<Kernel::SyscallNr::OPEN> data;
	data.path = path;
	data.flags = flags;

	/* Perform 'open' syscall */
	Userspace::syscall( Kernel::SyscallNr::OPEN , &data );

	if( data.status == Kernel::SyscallData<Kernel::SyscallNr::OPEN>::Status::SUCCESS ) {
		/* Return file descriptor of the file successfully opened */
		return data.fd;
	} else if( data.status == Kernel::SyscallData<Kernel::SyscallNr::OPEN>::Status::NOT_FOUND ) {
		/* No such file or directory */
		errno = ENOENT;
	} else {
		/* I/O error */
		errno = EIO;
	}
	/* Something went wrong so indicate error */
	return EOF;
#endif
}

void * sbrk( intptr_t increment ) {
    #ifndef __DISABLE_WARNINGS
    #warning "sbrk() not implemented yet."
    #endif
}

ssize_t write( int fd, const void * buffer, size_t size ) {
#if false	
	using namespace ReVolta;

	ssize_t retval { -EBADF };

	Kernel::SyscallData<Kernel::SyscallNr::WRITE> data;
	data.fd = fd;
	data.buffer = buffer;
	data.size = size;

	/* Perform 'write' syscall */
	ReVolta::Userspace::syscall( Kernel::SyscallNr::WRITE, &data );

	/* TODO: Shall return -1 in case of error, number of bytes written (?) otherwise */
	/* Return number of bytes written to the file descriptor */
	return data.bytes_written;
#endif
}