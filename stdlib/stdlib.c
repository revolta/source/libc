#include <stdlib.h>
#include <signal.h>

__attribute__((__noreturn__))
void abort( void ) {
#if defined( __IS_REVOLTA_KERNEL )
    /* TODO: Add proper kernel panic */
#else
    /* TODO: The parameter values are just dummy ones */
    //kill( 0, 0 );

#endif
    while( 1 ) {}
    __builtin_unreachable();
}

int atexit( void (*)( void ) ) {

}